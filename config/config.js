exports.kmToRadian = (miles) => {
  var earthRadiusInMiles = 6378;
  return miles / earthRadiusInMiles;
};

exports.distance = (latlong1, latlong2, unit = "N") => {
  if (latlong1[0] == latlong2[0] && latlong1[0] == latlong2[0]) {
    return 0;
  } else {
    var radlat1 = (Math.PI * latlong1[0]) / 180;
    var radlat2 = (Math.PI * latlong2[0]) / 180;
    var theta = latlong1[1] - latlong2[1];
    var radtheta = (Math.PI * theta) / 180;
    var dist =
      Math.sin(radlat1) * Math.sin(radlat2) +
      Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit == "K") {
      dist = dist * 1.609344;
    }
    if (unit == "N") {
      dist = dist * 0.8684;
    }
    return dist;
  }
};
