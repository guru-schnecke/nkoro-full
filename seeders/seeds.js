const Restaurant = require("../models/restaurant.model");
const Cuisine = require("../models/cuisine.model");
//mongoose connection
require("../config/db_connect");

Restaurant.deleteMany({})
  .then(() => {
    let cuisine = new Cuisine({ name: "Igbo" });
    cuisine
      .save()
      .then(() => {
        console.log("cuisine: ", cuisine);
        Restaurant.create([
          {
            cuisines: [cuisine._id],
            rating: {
              price: 4.0,
              clean: 4.0,
              service: 3.0,
              taste: 2.0,
              final: 3.0,
            },
            image: "https://hintng.com/wp-content/uploads/2018/06/rice2.jpg",
            isActive: true,
            isFeatured: false,
            name: "meaty cardi",
            slug: "meaty-cardi",
            location: { type: "Point", coordinates: [39.174466, 21.516588] },
          },
          {
            cuisines: [cuisine._id],
            rating: {
              price: 4.0,
              clean: 4.0,
              service: 3.0,
              taste: 2.0,
              final: 3.0,
            },
            image:
              "https://4.bp.blogspot.com/-Aw9h8xJTeKM/W8IIB86NQbI/AAAAAAAAR1U/wZlZVtxZOyIHQiHNCXxDZpDMvjKXM1upwCLcBGAs/s1600/Amala.jpg",
            isActive: true,
            isFeatured: false,
            name: "wicked",
            slug: "wicked",
            location: { type: "Point", coordinates: [39.15815, 21.57321] },
          },
          {
            cuisines: [cuisine._id],
            rating: {
              price: 4.0,
              clean: 4.0,
              service: 3.0,
              taste: 2.0,
              final: 3.0,
            },
            image:
              "https://www.zikoko.com/wp-content/uploads/zikoko/cloudinary/v1472391469/ojeebcwscoa3lga6gnff.jpg",
            isActive: true,
            isFeatured: false,
            name: "burgerz",
            slug: "burgerz",
            location: { type: "Point", coordinates: [39.14785, 21.57559] },
          },
        ])
          .then((suc) => {
            console.log("Multiple documents inserted to Collection");
            console.log("ctrl + c to quit");
          })
          .catch((ers) => {
            console.error(ers);
            console.log(ers);
          });
      })
      .catch((er) => {
        console.log(er);
      });
  })
  .then(() => {
    // process.exit();
  });
