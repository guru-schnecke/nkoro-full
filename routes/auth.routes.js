const router = require("express").Router();
const gravatar = require("gravatar");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/user.model");
const errorCode = require("../config/code");

require("dotenv").config();
/* 
    @route    POST api/auth/register
    @desc     Email, Phone Signup user & get token
    @access   Public
*/
router.post("/register", async (req, res) => {
  try {
    let {
      first_name,
      last_name,
      email,
      username,
      phone,
      password,
      user_type,
    } = req.body;

    const avatar = gravatar.url(email, {
      s: "200",
      r: "pg",
      d: "mm",
    });

    let user = new User({
      first_name,
      last_name,
      username,
      email,
      phone,
      avatar,
      password,
    });
    //check user type
    if (user_type != "admin" || user_type != "owner") {
      user.user_type = "normal";
    }
    //hash password
    user.password = await bcrypt.hash(password, 10);

    await user.save();

    console.log(user);
    const payload = {
      user: {
        id: user._id,
      },
    };

    //sign token
    jwt.sign(
      payload,
      process.env.SECRET,
      { expiresIn: 360000 * 60 * 60 },
      (err, token) => {
        if (err) throw err;
        res.json({ token }).status(200);
      }
    );
  } catch (error) {
    console.log(error);
    res.json({}).status(400);
  }
});

/*
    @route    POST api/auth/login
    @desc     Authenticate user & get token
    @access   Public
*/
router.post("/login", async (req, res) => {
  const { phone, password } = req.body;

  try {
    let user = await User.findOne({ phone });

    if (!user) {
      return res.status(400).json(errorCode.badCredentials);
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      return res.status(400).json(errorCode.badCredentials);
    }

    const payload = {
      user: {
        id: user._id,
      },
    };

    jwt.sign(
      payload,
      process.env.SECRET,
      { expiresIn: 360000 },
      (err, token) => {
        if (err) throw err;
        res.json({ token }).status(200);
      }
    );
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ message: "Server error" });
  }
});

module.exports = router;
