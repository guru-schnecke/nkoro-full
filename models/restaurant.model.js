const mongoose = require("mongoose");

const pointSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ["Point"],
    required: true,
  },
  coordinates: {
    type: [Number],
    required: true,
  },
});

const restaurantSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      lowercase: true,
    },
    location: {
      type: pointSchema,
      required: true,
    },
    image: String,
    slug: String,
    isActive: {
      type: Boolean,
      default: false,
    },
    isFeatured: {
      type: Boolean,
      default: false,
    },
    city: String,
    country: String,
    rating: {
      price: {
        type: Number,
        default: 0.0,
      },
      clean: {
        type: Number,
        default: 0.0,
      },
      service: {
        type: Number,
        default: 0.0,
      },
      taste: {
        type: Number,
        default: 0.0,
      },
      final: {
        type: Number,
        default: 0.0,
      },
    },
    comments: [
      {
        user: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "User",
        },
        comment: String,
        createdAt: {
          type: Date,
          default: Date.now,
        },
        rating: {
          price: {
            type: Number,
            default: 0.0,
          },
          clean: {
            type: Number,
            default: 0.0,
          },
          service: {
            type: Number,
            default: 0.0,
          },
          taste: {
            type: Number,
            default: 0.0,
          },
        },
      },
    ],
    cuisines: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Cuisine",
      },
    ],
  },
  { timestamps: true }
);

restaurantSchema.index({ location: "2dsphere" });

const Restaurant = mongoose.model("Restaurant", restaurantSchema);
module.exports = Restaurant;
