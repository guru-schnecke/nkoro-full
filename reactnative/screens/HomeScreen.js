import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View, ActivityIndicator } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { TextInput, ScrollView } from "react-native-gesture-handler";
import { Image, Badge } from "react-native-elements";
import imgs from "../assets/img/bounce1.jpg";
import imgss from "../assets/img/bounce2.jpg";
import imgsss from "../assets/img/bounce3.jpg";
import imgssss from "../assets/img/bounce4.jpg";

import Header from "../components/Header";
import NearByNkoro from "../components/NearByNkoro";

import { getFeaturedNkoro } from "../utliz/api";

const HomeScreen = ({ navigation }) => {
  const [nkoros, setNkoros] = useState([]);

  // getNkoros();
  useEffect(() => {
    let mounted = true;

    getFeaturedNkoro()
      .then((data) => {
        // console.log(data.data.restaurants);
        if (mounted) {
          setNkoros(data.data.restaurants);
        }
      })
      .catch((er) => {
        console.log(er);
      });

    // function nearByNkoros() {
    //   // setLoading(true);
    //   closeBy().then((data) => {
    //     if (mounted) {
    //       setNkoros(data.docChanges());
    //     }

    //     // setLoading(false);
    //   });
    // }
    // nearByNkoros();
    return () => {
      mounted = false;
    };
  }, [nkoros, setNkoros]);

  // function nearByNkoros() {
  //   closeBy().then((data) => {
  //     setNkoros(data.docChanges());
  //   });
  // }

  return (
    <View style={styles.container}>
      <ScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        alwaysBounceVertical={true}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.header}>
          <View style={{ flex: 3 }}>
            <Header home={true} />
          </View>
          <View style={styles.searchContainer}>
            <Text style={{ fontSize: 34, color: "#FFF", fontWeight: "bold" }}>
              Welíköm
            </Text>
            <Text style={{ color: "#FFF", fontSize: 18 }}>
              Wetín u wan chop?
            </Text>
          </View>
        </View>
        <View style={styles.content}>
          <View>
            <ScrollView
              horizontal={true}
              alwaysBounceHorizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <View style={styles.popular}>
                <Image
                  source={imgs}
                  style={{
                    width: null,
                    height: "100%",
                    resizeMode: "cover",
                  }}
                  PlaceholderContent={<ActivityIndicator />}
                />
              </View>
              <View style={styles.popular}>
                <Image
                  source={imgss}
                  style={{
                    width: null,
                    height: "100%",
                    resizeMode: "cover",
                  }}
                  PlaceholderContent={<ActivityIndicator />}
                ></Image>
              </View>
              <View style={styles.popular}>
                <Image
                  source={imgsss}
                  style={{
                    width: null,
                    height: "100%",
                    resizeMode: "cover",
                  }}
                  PlaceholderContent={<ActivityIndicator />}
                />
              </View>
              <View style={styles.popular}>
                <Image
                  source={imgssss}
                  style={{
                    width: null,
                    height: "100%",
                    borderRadius: 150 / 2,
                    resizeMode: "cover",
                  }}
                  PlaceholderContent={<ActivityIndicator />}
                />
              </View>
            </ScrollView>
          </View>
          <View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginVertical: 10,
              }}
            >
              <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                Trending
                <Text
                  style={{ fontSize: 15, color: "gray", fontWeight: "normal" }}
                >
                  {" "}
                  - 8 dey hot
                </Text>
              </Text>
              <Text>More</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                flexWrap: "wrap",
              }}
            >
              {nkoros.length > 0 ? (
                nkoros.map((nkoro, index) =>
                  index < 4 ? <NearByNkoro key={index} info={nkoro} /> : null
                )
              ) : (
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignContent: "center",
                  }}
                >
                  <ActivityIndicator size={30} />
                  <Text>No Hot Places</Text>
                </View>
              )}
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#E2445B",
    flex: 1,
  },
  header: {
    paddingTop: 30,
    // flex: 2,
    height: 290,
    marginHorizontal: 10,
  },
  searchContainer: {
    // color: "#fff",
    // flexDirection: "row",
    justifyContent: "center",
    // alignItems: "center",
    flex: 1,
    // borderRadius: 10,
    marginTop: 15,
    paddingVertical: 9,
    paddingHorizontal: 10,
    marginBottom: 15,
  },
  content: {
    backgroundColor: "#F7F6F6",
    // marginHorizontal: 10,
    paddingHorizontal: 15,
    paddingTop: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    flex: 4,
  },
  popular: {
    width: 150,
    height: 150,
    borderRadius: 10,
    backgroundColor: "red",
    marginRight: 10,
    overflow: "hidden",
  },
});
