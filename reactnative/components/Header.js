import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View, AsyncStorage } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Icon } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";

const Header = ({ color, home }) => {
  const [city, setcity] = useState("");
  const [isLoading, setLoading] = useState(true);
  const navigation = useNavigation();
  if (!color) {
    color = "#FFF";
  }

  const _retrieveData = async () => {
    // setLoading(fa);
    try {
      const value = await AsyncStorage.getItem("city");
      // console.log("value: ", value);
      if (value !== null) {
        console.log("value: ", value);
        setcity(value);
        setLoading(false);
      } else {
        setcity("Nigeria");
      }
      // console.log("city", value);

      // We have data!!
      // console.log("city", value);
    } catch (error) {
      // Error retrieving data
      console.log("error", error);
    }
  };

  useEffect(() => {
    _retrieveData();
  }, [_retrieveData]);

  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        marginVertical: 15,
        paddingHorizontal: 10,
      }}
    >
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        {home ? (
          <>
            <Icon
              name="map-marker"
              size={30}
              type="font-awesome"
              color={color}
            />
            <Text
              style={{
                fontSize: 18,
                color: color,
                // fontWeight: "bold",
                marginLeft: 5,
              }}
            >
              {city ? city : "Nigeria"}
            </Text>
          </>
        ) : (
          <>
            <Icon
              name="arrow-left"
              size={30}
              type="font-awesome"
              color={color}
              onPress={() => navigation.goBack()}
            />
          </>
        )}
      </View>
      <TouchableOpacity>
        <Icon name="bars" size={30} type="font-awesome" color={color} />
      </TouchableOpacity>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({});
