import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import { Image, Badge, Icon, Rating } from "react-native-elements";
import imgs from "../assets/img/bounce1.jpg";
import imgss from "../assets/img/bounce2.jpg";
import { useNavigation } from "@react-navigation/native";

const NearByNkoro = ({ odd, info }) => {
  const navigation = useNavigation();
  const details = {
    id: info._id,
    name: info.name,
    cuisine: info.cuisines,
    image: info.image,
    rating: info.rating,
    distance: info.distance,
    comments: info.comments,
  };

  // console.log(details);
  return (
    <TouchableOpacity
      style={styles.aroundMeOdd}
      onPress={() => navigation.navigate("Nkoro", { details })}
    >
      {/* <View style={styles.aroundMeOdd}> */}
      <View
        style={{
          flex: 5,
          borderTopLeftRadius: 9,
          borderTopRightRadius: 9,
          overflow: "hidden",
        }}
      >
        <Image
          source={odd ? imgs : { uri: info.image }}
          style={{ width: null, height: 150, resizeMode: "cover" }}
          PlaceholderContent={<ActivityIndicator />}
        >
          <View style={styles.overlay}></View>
        </Image>
      </View>
      <View
        style={{
          flex: 1,
          marginHorizontal: 5,
          marginVertical: 10,
          paddingLeft: 10,
          paddingRight: 10,
        }}
      >
        <Text
          style={{
            fontSize: 15,
            fontWeight: "bold",
            textTransform: "capitalize",
          }}
        >
          {info.name}
        </Text>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <View style={{ flexDirection: "row" }}>
            {info.cuisines
              ? info.cuisines.map((el, i) => (
                  <Text key={i}>{i < 4 ? el.name : ""} </Text>
                ))
              : null}
          </View>
          <View style={{ flexDirection: "row" }}>
            <Icon name="star" size={15} color="#FFDF00" type="font-awesome" />
            <Text>{info.rating ? info.rating.final : 0}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default NearByNkoro;

const styles = StyleSheet.create({
  holder: {
    width: "50%",
  },
  overlay: {
    position: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "black",
    opacity: 0.3,
  },
  aroundMeOdd: {
    // flex: 1,
    width: "100%",
    borderColor: "#cecece",
    borderWidth: 0.5,
    height: 150,
    marginRight: 10,
    // flexDirection: "row",
    backgroundColor: "white",
    borderRadius: 9,
    // height: 100,
    marginBottom: 10,
    paddingBottom: 15,
    elevation: 4,
  },
});
